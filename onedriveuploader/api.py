import onedrivesdk
from .utils.query_redirect_server import get_callback_result

import re
from os.path import expanduser, dirname, basename
import logging


class OneDriveClient:
    SCOPES = ['wl.signin', 'wl.offline_access', 'onedrive.readwrite']
    SESSION_PATH = expanduser("~/.onedriveuploader")

    logger = logging.getLogger(__name__)

    def __init__(self, client_id, client_secret, redirect_url):
        self._client_id = client_id
        self._client_secret = client_secret
        self._redirect_url = redirect_url

        self._client = onedrivesdk.get_default_client(
            client_id=self._client_id, scopes=OneDriveClient.SCOPES)
        try:
            self.logger.info("try to load session file")
            self._client.auth_provider.load_session(
                    path=OneDriveClient.SESSION_PATH)
        except FileNotFoundError:
            self.logger.info("no such session file")
            pass  # no session file

    def auth(self):
        "Get a new auth code and redeem access token & refresh token"

        url = self._client.auth_provider.get_auth_url(self._redirect_url)

        print("Open this URL in your browser: " + url)
        result = get_callback_result(url=self._redirect_url)

        try:
            self._client.auth_provider.authenticate(
                result['code'], self._redirect_url, self._client_secret)
            self._client.auth_provider.save_session(
                path=OneDriveClient.SESSION_PATH)
        except KeyError:  # auth error
            raise(ApiError(result['error'], result['error_description']))
        except Exception as e:  # redeem error
            if str(e) == 'invalid_grant':
                raise(ApiError(e, 'May be the auth code is invalid.'))

    def refresh_token(self):
        "Refresh access token"

        try:
            self._client.auth_provider.refresh_token()
        except RuntimeError as e:
            raise(ApiError(e, 'May be the auth code is invalid.'))

    def check_auth(self):
        "If refresh access token successfully"

        try:
            self.refresh_token()
            return True
        except ApiError:
            return False

    def upload(self, path, src):
        base_dir, name = OneDriveClient._split_dir_and_base(path)
        base_dir = OneDriveClient._path_add_root(base_dir)
        self._client.item(path=base_dir).children[name].upload(src)

    def mkdir(self, path):
        base_dir, name = OneDriveClient._split_dir_and_base(path)
        base_dir = OneDriveClient._path_add_root(base_dir)

        f = onedrivesdk.Folder()
        i = onedrivesdk.Item()
        i.name = name
        i.folder = f

        return self._client.item(path=base_dir).children.add(i)

    @staticmethod
    def _path_add_root(path):
        return re.sub('^/{0,1}', '/', path)

    @staticmethod
    def _split_dir_and_base(path):
        return (dirname(path), basename(path))


class ApiError(RuntimeError):
    def __init__(self, error, error_description='none'):
        self.error = error
        self.error_description = error_description

    def __str__(self):
        return '{}: {}'.format(self.error, self.error_description)
