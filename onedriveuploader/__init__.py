from .api import OneDriveClient
import click


@click.command(name='onedriveuploader')
@click.option('--client-id', type=str)
@click.option('--client-secret', type=str)
@click.option('--redirect-url', type=str)
@click.option('--path', type=str)
@click.option('--src', type=str)
def upload(client_id, client_secret, redirect_url, path, src):
    client = OneDriveClient(client_id, client_secret, redirect_url)
    if not client.check_auth():
        if not client.auth():
            return False

    if path is not None and src is not None:
        client.upload(path, src)

    return True
