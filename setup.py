from setuptools import setup, find_packages


setup(
    name='onedriveuploader',
    version='1.0',
    author='c0ldcat',
    packages=find_packages(),
    scripts=['bin/onedrive-uploader'],
    install_requires=[
        'onedrivesdk',
        'click'
    ]
)
