# OneDrive Uploader
Simple OneDrive uploader use for backup server

# Installation
``` shell
python setup.py install
```

# Requirment
A registered app in [Microsoft Application Registration Portal](https://apps.dev.microsoft.com/?referrer=https%3A%2F%2Fdev.onedrive.com) is required for connecting with Microsoft Graph. Find more details in [Registering your app for Microsoft Graph](https://docs.microsoft.com/en-us/onedrive/developer/rest-api/getting-started/app-registration#register-your-app-with-microsoft).

# Usage
```
Usage: onedrive-uploader [OPTIONS]

Options:
  --client-id TEXT
  --client-secret TEXT
  --redirect-url TEXT
  --path TEXT
  --src TEXT
  --help                Show this message and exit.
```

The program also read options from environment variables. For example:

``` shell
ONEDRIVE_CLIENT_ID='' ONEDRIVE_CLIENT_SECRET='' ONEDRIVE_REDIRECT_URL='' onedrive-uploader --path '/file' --src file
```

If path or src is none, it will just request the access permission.

# ToDo
* [ ] Better log
* [x] Dockerfile example
* [ ] Use another way to show login url
