FROM alpine:3.7

RUN apk add --no-cache python3

WORKDIR /src
COPY . onedriveuploader
RUN cd onedriveuploader \
 && python3 setup.py install \
 && cp docker-entrypoint.sh /docker-entrypoint.sh \
 && cd .. \
 && rm -rf onedriveuploader
RUN chmod +x /docker-entrypoint.sh

ENV ONEDRIVE_CLIENT_ID ''
ENV ONEDRIVE_CLIENT_SECRET ''
ENV ONEDRIVE_REDIRECT_URL ''

ENTRYPOINT ["/docker-entrypoint.sh"]
